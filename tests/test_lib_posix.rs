mod posix_tests {
    use relish::aux::args_from_ast;
    use relish::stdlib::{dynamic_stdlib, static_stdlib};
    use relish::ast::{lex, eval, SymTable};

    #[test]
    fn test_cmd_singlet() {
        let document = "(binary)";
        let result = vec!["binary"];

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ok(ref s) = lex(&document.to_string()) {
            assert_eq!(
                args_from_ast(s, &mut syms),
                result
            )
        } else {
            panic!()
        }
    }

    #[test]
    fn test_cmd_list() {
        let document = "(binary --flag=1 122 'yeet' true)";
        let result = vec!["binary", "--flag=1", "122", "yeet", "true"];

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ok(ref s) = lex(&document.to_string()) {
            assert_eq!(
                args_from_ast(s, &mut syms),
                result
            )
        } else {
            panic!()
        }
    }

    #[test]
    fn test_cmd_syms_undef() {
        let document = "(binary --flag=1 122 'yeet' true syms)";
        let result = vec!["binary", "--flag=1", "122", "yeet", "true", "syms"];

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ok(ref s) = lex(&document.to_string()) {
            assert_eq!(
                args_from_ast(s, &mut syms),
                result
            )
        } else {
            panic!()
        }
    }

    #[test]
    fn test_cmd_syms_unwrap_simple() {
        let decl = "(def syms '' 1)";
        let document = "(binary --flag=1 122 'yeet' true syms)";
        let result = vec!["binary", "--flag=1", "122", "yeet", "true", "1"];

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        eval(&lex(&decl.to_string()).unwrap(), &mut syms).unwrap();

        if let Ok(ref s) = lex(&document.to_string()) {
            assert_eq!(
                args_from_ast(s, &mut syms),
                result
            )
        } else {
            panic!()
        }
    }

    #[test]
    fn test_cmd_syms_unwrap_eval() {
        let document = "(binary --flag=1 122 'yeet' true (add 1 2))";
        let result = vec!["binary", "--flag=1", "122", "yeet", "true", "3"];

        let mut syms = SymTable::new();
        static_stdlib(&mut syms);
        dynamic_stdlib(&mut syms, None);

        if let Ok(ref s) = lex(&document.to_string()) {
            assert_eq!(
                args_from_ast(s, &mut syms),
                result
            )
        } else {
            panic!()
        }
    }
}
