mod str_lib_tests {
    use relish::ast::{eval, lex, Ctr, FTable, VTable};
    use relish::stdlib::get_stdlib;
    use std::cell::RefCell;
    use std::rc::Rc;

    #[test]
    fn test_simple_concat() {
        let document = "(concat 'test')";
        let result = "test";
        let vt = Rc::new(RefCell::new(VTable::new()));
        let ft: Rc<RefCell<FTable>>;
        match get_stdlib(vt.clone()) {
            Ok(f) => ft = f,
            Err(s) => {
                ft = Rc::new(RefCell::new(FTable::new()));
                println!("Couldnt get stdlib: {}!", s);
                assert!(false)
            }
        }

        match lex(document.to_string()) {
            Err(s) => {
                println!("Couldnt lex {}: {}\n", document, s);
                assert!(false);
            }

            Ok(tree) => match eval(tree, vt.clone(), ft.clone(), false) {
                Err(s) => {
                    println!("Couldnt eval {}: {}\n", document, s);
                    assert!(false);
                }

                Ok(ctr) => match ctr {
                    Ctr::Symbol(_) => assert!(false),
                    Ctr::String(s) => assert_eq!(s, result),
                    Ctr::Integer(_) => assert!(false),
                    Ctr::Float(_) => assert!(false),
                    Ctr::Bool(_) => assert!(false),
                    Ctr::Seg(_) => assert!(false),
                    Ctr::None => assert!(false),
                },
            },
        }
    }

    #[test]
    fn test_poly_concat() {
        let document = "(concat 'test' 1 2 3)";
        let result = "test123";
        let vt = Rc::new(RefCell::new(VTable::new()));
        let ft: Rc<RefCell<FTable>>;
        match get_stdlib(vt.clone()) {
            Ok(f) => ft = f,
            Err(s) => {
                ft = Rc::new(RefCell::new(FTable::new()));
                println!("Couldnt get stdlib: {}!", s);
                assert!(false)
            }
        }

        match lex(document.to_string()) {
            Err(s) => {
                println!("Couldnt lex {}: {}\n", document, s);
                assert!(false);
            }

            Ok(tree) => match eval(tree, vt.clone(), ft.clone(), false) {
                Err(s) => {
                    println!("Couldnt eval {}: {}\n", document, s);
                    assert!(false);
                }

                Ok(ctr) => match ctr {
                    Ctr::Symbol(_) => assert!(false),
                    Ctr::String(s) => assert_eq!(s, result),
                    Ctr::Integer(_) => assert!(false),
                    Ctr::Float(_) => assert!(false),
                    Ctr::Bool(_) => assert!(false),
                    Ctr::Seg(_) => assert!(false),
                    Ctr::None => assert!(false),
                },
            },
        }
    }

    #[test]
    fn test_empty_concat() {
        let document = "(concat)";
        let result = "";
        let vt = Rc::new(RefCell::new(VTable::new()));
        let ft: Rc<RefCell<FTable>>;
        match get_stdlib(vt.clone()) {
            Ok(f) => ft = f,
            Err(s) => {
                ft = Rc::new(RefCell::new(FTable::new()));
                println!("Couldnt get stdlib: {}!", s);
                assert!(false)
            }
        }

        match lex(document.to_string()) {
            Err(s) => {
                println!("Couldnt lex {}: {}\n", document, s);
                assert!(false);
            }

            Ok(tree) => match eval(tree, vt.clone(), ft.clone(), false) {
                Err(s) => {
                    println!("Couldnt eval {}: {}\n", document, s);
                    assert!(false);
                }

                Ok(ctr) => match ctr {
                    Ctr::Symbol(_) => assert!(false),
                    Ctr::String(s) => assert_eq!(s, result),
                    Ctr::Integer(_) => assert!(false),
                    Ctr::Float(_) => assert!(false),
                    Ctr::Bool(_) => assert!(false),
                    Ctr::Seg(_) => assert!(false),
                    Ctr::None => assert!(false),
                },
            },
        }
    }
}
